#!/usr/bin/python2.5
#!/usr/bin/python2.5
# partest.py
# 21 June 2007

from position import *
from search import *
import sys, thread, time, os

try:
    fen = sys.argv[1]
    pos = position(fen)
except:
    pos = ipos

def removePV(res):
    x = res.copy()
    del x['pv']
    return x

def main(Max = 32):
    for d in range(Max):
        print "\nDepth %s:" % d
        if 'CLEAR' in os.environ:
            pvCache.clear()
        if 'PARALLEL' in os.environ:
            splits = 1
        else:
            splits = 0
        x = alphaBeta(pos.copy(), d, top = True, splits = splits)
        print "    ", removePV(x)
        print "  ", algebraicForPV(x['pv'])

def main2(Max = 32):
    global pvCache
    otherPVCache = {}
    times = [1, 1]

    for d in range(Max):
        print "\nDepth %s:" % d
        for splits in (0, 1):
            x = alphaBeta(pos, d, top = True, splits = splits)
            print ["no split", "one split"][splits]
            print "    ", removePV(x)
            print "  ", algebraicForPV(x['pv'])

            times[splits] = x['time']
            pvCache, otherPVCache = otherPVCache, pvCache
        speedup = float(times[0]) / times[1]
        print "Speed-up: %.2fx   (ratio: %.1f%%)" % (speedup, 100.0 / 
speedup)

if __name__ == '__main__':
    thread.start_new_thread(main2, ())
    while 1:
        time.sleep(1000)
