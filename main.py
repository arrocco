# main.py

"""
    This file is part of Arrocco, which is Copyright 2007 Thomas Plick 
    (tomplick 'at' gmail.com).

    Arrocco is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Arrocco is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from position import *
import thread, time

import sys, os

fen = (sys.argv + ["-"])[1]


def strangeKey((x, y)):
    if x == 'pv': return ('zzzzzzz', y)
    else:         return (x, y)


def f():
    if fen == '-':  pos = ipos
    else:           pos = position(fen)
        
    result = {'pv' : []}
    for depth in range(0, 1000):
        print "\n"
        a = time.time()
        result = alphaBetaWithOldPV(pos, depth, oldVariation = result['pv'])
        b = time.time()
        for (k, v) in sorted(result.items(), key = strangeKey):
            if k != 'pv':
                if k == 'value' and pos.turn == 1:
                    v *= -1
                print "%s: %s" % (k, v)
        print "Variation: %s" % algebraicForPV(result['pv'])
        print "Took %s seconds" % (int(b-a)+1)
        
    global running
    running = False
    sys.exit(0)
        
        
def alphaBetaWithOldPV2(pos, depth, oldVariation):
    arr = (Position * len(oldVariation))()
    arr[:] = oldVariation
    children = (Position * (depth * 512))()
    counter = int64(0)
    
    value = lib.alphaBetaWithOldPV(ctypes.byref(pos), children, 
                depth, -99999, 99999, ctypes.byref(counter), arr, len(oldVariation))
                
    L = []
    node = pos
    for i in range(depth + 1):
        L.append(node)
        try:
            node = node.children()[self.branch[i]]
        except:
            pass
            
    return dict(depth = depth, value = value, pv = L, nodeCount = counter.value)

running = True
if __name__ == '__main__':
    thread.start_new_thread(f, ())
    while running:
        time.sleep(1000)
