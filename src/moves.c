/*  moves.c
    1 Mar 2007 */

/*
    This file is part of Arrocco, which is Copyright 2007 Thomas Plick 
    (tomplick 'at' gmail.com).

    Arrocco is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Arrocco is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <time.h>
    
#define   BUT   &&
#define   ON_BOARD(x)   (x >= 0 && x < 8)
    
// These are in _bits.c.
extern const Int64 kingFields[64];
extern const Int64 knightFields[64];
extern const Int64 pawnMoveFields[2][64];
extern const Int64 pawnCaptureFields[2][64];
// extern const int areSquaresAdjacent[64][64];

const Int64 pawnFirstRowMask  = (255ULL <<  8) | (255ULL << 48);
const Int64 pawnSecondRowMask = (255ULL << 16) | (255ULL << 40);
const Int64 pawnThirdRowMask  = ~((255ULL << 24) | (255ULL << 32));


// For the lowest bit functions, we assume x > 0.

#ifdef ASM
#warning "Using ASM bit operations.  If this does not work, try 'make slow'."

int lowestBit(const Int64 x){
    return __builtin_ctzll(x);
}
int highestBit(const Int64 x){
    return 63 - __builtin_clzll(x);
}

#else

int lowestBit(const Int64 x){
    int i;
    for (i = 0; i < 64; i++){
        if (x & (1ULL << i)) return i;
    }
    return 0;
}

int highestBit(const Int64 x){
    int i;
    for (i = 63; i >= 0; i--){
        if (x & (1ULL << i)) return i;
    }
    return 0;
}

#endif  /*  ifdef ASM  */


int * extractBitsInto(Int64 bitfield, int * dests){
    while (bitfield){
        const int dst = lowestBit(bitfield);
        *dests++ = dst;
        bitfield ^= TWOTOTHE(dst);
    }
    return dests;
}

Position * extractBitsAsMoves(const Position * const restrict pos, const int src, Int64 bitfield,
                    Position * restrict child){
    Position half = *pos;
    const int piece = half.board[src];
    setPieceAt(&half, src, 0);
    half.turn ^= 1;
    
    const char * board = pos->board;
    
    while (bitfield){
        const int dst = lowestBit(bitfield);
        
        *child = half;
        setPieceAt(child, dst, piece);
        
        const int captureWorth = pieceValues[board[dst]];
        child->value[0] -= captureWorth;
        child->value[1] += captureWorth;
        child++;
        
        bitfield ^= TWOTOTHE(dst);
    }
    return child;
}

#define MOVE_FUNCTION(x) \
    Position * x(const Position * const restrict pos, const int src, \
                       Position * restrict children, \
                 const Int64 ownerField, const Int64 opponentField)
#define DEST_FUNCTION(x) \
    Int64 x(const Position * const restrict pos, const int src, \
            const Int64 occupancy[4])


MOVE_FUNCTION(kingMoves){
    Int64 bitfield = kingFields[src] & ~(opponentField | ownerField);
    return extractBitsAsMoves(pos, src, bitfield, children);
}
MOVE_FUNCTION(kingCaptures){
    Int64 bitfield = kingFields[src] & opponentField;
    return extractBitsAsMoves(pos, src, bitfield, children);
}
DEST_FUNCTION(kingDestinations){ return kingFields[src]; }


MOVE_FUNCTION(knightMoves){
    Int64 bitfield = knightFields[src] & ~(opponentField | ownerField);
    return extractBitsAsMoves(pos, src, bitfield, children);
}
MOVE_FUNCTION(knightCaptures){
    Int64 bitfield = knightFields[src] & opponentField;
    return extractBitsAsMoves(pos, src, bitfield, children);
}
DEST_FUNCTION(knightDestinations){ return knightFields[src]; }


int promotionPieces[4] = {10, 4, 8, 6};
Position * makePromotions(Position * children, const int howMany, 
                const Int64 moveField, const int side){
                
    memcpy(children + howMany, children, howMany * sizeof(Position));
    memcpy(children + 2 * howMany, children, howMany * 2 * sizeof(Position));
    int k;
    int dsts[2] = {lowestBit(moveField), highestBit(moveField)};
    for (k = 0; k < 4 * howMany; k++){
        setPieceAt(&children[k], dsts[k & 1], promotionPieces[k / howMany] | side);
    }
    return children + 4 * howMany;
}

MOVE_FUNCTION(pawnMoves){
    const int side = pos->board[src] & 1;
    Int64 moveField = pawnMoveFields[side][src] & ~(ownerField | opponentField);
    
    // If a pawn on its starting row is blocked, then it cannot move at all.
    //    The bit fields leave the possibility that a pawn can move two squares;
    //    we prevent that here.
    if ((pawnFirstRowMask & (one << src)) BUT !(moveField & pawnSecondRowMask)){
        moveField &= pawnThirdRowMask;
    }
    Position * newChildren = extractBitsAsMoves(pos, src, moveField, children);
    
    if (moveField && (src >> 3) == (side ? 1 : 6))
        return makePromotions(children, 1, moveField, side);
    else
        return newChildren;
}
MOVE_FUNCTION(pawnCaptures){
    const int side = pos->board[src] & 1;
    Int64 captureField = pawnCaptureFields[side][src] & opponentField;
    Position * newChildren = extractBitsAsMoves(pos, src, captureField, children);
    if (captureField && (src >> 3) == (side ? 1 : 6)){
        int howMany = (lowestBit(captureField) == highestBit(captureField)) ? 1 : 2;
        return makePromotions(children, howMany, captureField, side);
    } else
        return newChildren;
}
/* DEST_FUNCTION(pawnMoveDestinations){
    const int side = pos->board[src] & 1;
    Int64 moveField = pawnMoveFields[side][src] & ~(ownerField | opponentField);
    // If a pawn on its starting row is blocked, then it cannot move at all.
    //    The bit fields leave the possibility that a pawn can move two squares;
    //    we prevent that here.
    if ((pawnFirstRowMask & (one << src)) BUT !(moveField & pawnSecondRowMask)){
        moveField &= pawnThirdRowMask;
    }
    return moveField;
}
DEST_FUNCTION(pawnCaptureDestinations){
    const int side = pos->board[src] & 1;
    return pawnCaptureFields[side][src] & opponentField;
} */


extern const int x88FromDest[64];
static inline Position * simpleMoves(const Position * const restrict pos, 
          const int src, Position * restrict child, const int nds[4], const int ids[4]){
    int nd, id, i, dst, dindex;
    const char *destPiece;
    
    Position * origChild = child;
    
    Position half = *pos;
    const int piece = half.board[src];
    setPieceAt(&half, src, 0);
    half.turn ^= 1;
    
    for (i = 0; i < 4; i++){
        nd = nds[i];  id = ids[i];
        for (dst = x88FromDest[src] + nd,
             dindex = src + id,
             destPiece = (&pos->board[dindex]); 
                    !(dst & 0x88); 
                    dst += nd, dindex += id, destPiece += id){
            if (*destPiece == 0){
                *child = half;
                setPieceAt(child++, dindex, piece);
            } else if (sameColorNotZero(pos->board[src], *destPiece)){
                break;
            } else {
                *child = half;
                setPieceAt(child, dindex, piece);
                child->value[0] -= pieceValues[*destPiece];
                child->value[1] += pieceValues[*destPiece];
                
                // Make alphaBeta examine capture moves first:
                // Craftily (crappily?) reuse half for swapping..
                Position half = *origChild;
                *origChild = *child;
                *child = half;
                
                child++;
                break;
            }
        }
    }
    return child;
}


MOVE_FUNCTION(bishopMoves){
    static int nds[] = {17, 15, -15, -17};
    static int ids[] = {9,   7,  -7, -9};
    return simpleMoves(pos, src, children, nds, ids);
}


extern const Int64 rookHorizontalDests[64][256];
extern const Int64 rookVerticalDests[64][256];
extern const int rookVerticalShift[64];

DEST_FUNCTION(rookDestinations){
    Int64 dests  = rookVerticalDests[src][
                        (occupancy[1] >> rookVerticalShift[src]) & (255ULL)];
    return dests | rookHorizontalDests[src][
                        (occupancy[0] >> (src & ~7)) & (255ULL)];
}
MOVE_FUNCTION(rookMoves){
    Int64 occupancy[4];
    for (int i = 0; i < 4; i++)
        occupancy[i] = pos->white[i] | pos->black[i];
    Int64 dests  = rookDestinations(pos, src, occupancy);
    return extractBitsAsMoves(pos, src, 
                dests & ~occupancy[0],  children);
}
MOVE_FUNCTION(rookCaptures){
    Int64 occupancy[4];
    for (int i = 0; i < 4; i++)
        occupancy[i] = pos->white[i] | pos->black[i];
    Int64 dests  = rookDestinations(pos, src, occupancy);
    return extractBitsAsMoves(pos, src, 
                dests & opponentField,  children);
}


MOVE_FUNCTION(queenMoves){
    return rookMoves(pos, src, bishopMoves(pos, src, children, ownerField, opponentField),
                ownerField, opponentField);
}
MOVE_FUNCTION(queenCaptures){
    return rookCaptures(pos, src, children, ownerField, opponentField);
}


MOVE_FUNCTION(noMoves){    return children + 0; }
DEST_FUNCTION(noDestinations){    return 0ULL; }

typedef MOVE_FUNCTION((*MoveFunction));
MoveFunction moveFunctions[14] = {noMoves, noMoves, pawnMoves, pawnMoves,
        knightMoves, knightMoves, bishopMoves, bishopMoves, rookMoves, rookMoves, 
        queenMoves, queenMoves, kingMoves, kingMoves};
MoveFunction captureFunctions[14] = {noMoves, noMoves, pawnCaptures, pawnCaptures,
        knightCaptures, knightCaptures, noMoves, noMoves, rookCaptures, rookCaptures, 
        queenCaptures, queenCaptures, kingCaptures, kingCaptures};


static inline Position * nextMoveChildren(const Position * const restrict pos, 
                Position * restrict children, Int64 * piecePlaces,
                const Int64 ownerField, const Int64 opponentField){
    const int src = lowestBit(*piecePlaces);
    *piecePlaces ^= TWOTOTHE(src);
    return moveFunctions[pos->board[src]](pos, src, children, ownerField, opponentField);
}

static inline Position * nextCaptureChildren(const Position * const restrict pos, 
                Position * restrict children, Int64 * piecePlaces,
                const Int64 ownerField, const Int64 opponentField){
    const int src = lowestBit(*piecePlaces);
    *piecePlaces ^= TWOTOTHE(src);
    return captureFunctions[pos->board[src]](pos, src, children, ownerField, opponentField);
}

// XXX Pass occupancyField too?  It is used a lot, and regenerating it all the time
//      may be wasteful...
int makeChildren(const Position * const restrict pos, 
                       Position * const restrict children)
{    
    static int src;
    Position * child = (Position *) children;
    Int64 ownerField    = pos->white[  pos->turn  << 2];
    Int64 opponentField = pos->white[(!pos->turn) << 2];
    Int64 piecePlaces = ownerField;
    
    while (piecePlaces){
        child = nextCaptureChildren(pos, child, &piecePlaces, ownerField, opponentField);
    }
    
    piecePlaces = ownerField;
    while (piecePlaces){
        child = nextMoveChildren(pos, child, &piecePlaces, ownerField, opponentField);
    }
    
    return child - children;
}


#include "check.c"
// favor longer variations... but how?
int alphaBeta(Position * const restrict pos, Position * const restrict children,
            const int depth,
            int alpha, const int beta, Int64 * const nodeCount,
            int * offswitch)
{
    pos->branchLength = 1;
    
    // early exits:
    if (*offswitch) return alpha;
    else if (pos->value[0] > 8000 || pos->value[1] > 8000){
        int returnValue = pos->value[pos->turn];
        // if (returnValue > 0)  returnValue += depth;
        // if (returnValue > 8000) return returnValue;
        // else  return -8000 - depth;
        returnValue = (returnValue > 0) ? 8000 : -8000;
        return returnValue;
    } else if (depth == 0){
        return pos->value[pos->turn];
    }
    
    Position * restrict child, * restrict endOfChildren;
    int bestChild;
    int bestBranch[32];
    const int bSize = (depth - 1) * sizeof(int);
    
    const Int64 ownerField    = pos->white[  pos->turn  << 2];
    const Int64 opponentField = pos->white[(!pos->turn) << 2];
    int value, i = 0, bestBranchSize = 0;

            // if (inCheck(child, pos->turn)){
            //    i++;
            //    continue;
            // }
    
#define ALPHABETA_LOOP_BODY(nextChildrenFunc) \
    while (piecePlaces){ \
        endOfChildren = nextChildrenFunc(pos, children, &piecePlaces, ownerField, opponentField); \
        for (child = children; child < endOfChildren; child++){ \
            value = -alphaBeta(child, children + 32, depth-1, -beta, -alpha, nodeCount, offswitch); \
            if (value >= beta){ \
                (*nodeCount) += i+1; \
                return beta; \
            } else if (value > alpha || (value == alpha && child->branchLength > bestBranchSize)){ \
                alpha = value; \
                bestChild = ++i; \
                memcpy(bestBranch, child->branch, bSize); \
                bestBranchSize = child->branchLength; \
            } else \
                ++i; \
        } \
    } \
    
    Int64 piecePlaces = ownerField;
    ALPHABETA_LOOP_BODY(nextCaptureChildren);
    piecePlaces = ownerField;
    ALPHABETA_LOOP_BODY(nextMoveChildren);
    
    if (i == 0){
        return pos->value[pos->turn];
    }
    
    pos->branch[0] = bestChild - 1;
    memcpy(&pos->branch[1], bestBranch, bSize);
    pos->branchLength = bestBranchSize + 1;
    
    (*nodeCount) += i;
    return alpha;
}

