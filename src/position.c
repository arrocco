/*  position.c
    28 Feb 2007 */

/*
    This file is part of Arrocco, which is Copyright 2007 Thomas Plick 
    (tomplick 'at' gmail.com).

    Arrocco is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Arrocco is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <string.h>
    
typedef   unsigned long long int   Int64;
const Int64 one = 1ULL;
    
typedef struct {
    char board[64];
    short castling[4];
    short turn;
    short epFile;
    
    short value[2];
    short branch[32];
    int branchLength;
    
    Int64 pieces[14];
    
    Int64 white[4], black[4];
    int isStale;
} Position;

int sizeOfPosition(){ return sizeof(Position); }


extern const Int64 twoToThe[64];
// #define TWOTOTHE(n) (twoToThe[n])
#define TWOTOTHE(n) (one << (n))


void newPosition(Position * pos){
    memset(pos, 0, sizeof(Position));
    pos->epFile = -1;
}

inline int getPieceAt(Position * pos, const int sq){
    return pos->board[sq];
}

// These are defined in Python.
extern const Int64 rotMasks[64][8];

static inline void maskSetEmptyPiece_inline(Int64 * restrict white, const int sq){
    int i;
    const Int64 * restrict squareMasks = rotMasks[sq];
    for (i = 0; i < 8; i++)
        // *white++ &= *squareMasks++;
        white[i] &= squareMasks[i];
}

void maskSetEmptyPiece(Int64 * const restrict white, 
                       Int64 * const restrict black, const int sq){
    maskSetEmptyPiece_inline(white, sq);
}
void maskSetWhitePiece(Int64 * const restrict white, 
                       Int64 * const restrict black, const int sq){
    int i;
    const Int64 * restrict squareMasks = rotMasks[sq];
    for (i = 0; i < 4; i++){
        black[i] &=  ~(white[i] |= ~squareMasks[i]);
    }
}
void maskSetBlackPiece(Int64 * const restrict white,
                       Int64 * const restrict black, const int sq){
    int i;
    const Int64 * restrict squareMasks = rotMasks[sq];
    for (i = 0; i < 4; i++){
        white[i] &=  ~(black[i] |= ~squareMasks[i]);
    }
}

typedef   void (*MaskFunction)(Int64 * const restrict white, 
                               Int64 * const restrict black, const int sq);
MaskFunction maskFunctions[14] = {maskSetEmptyPiece, maskSetEmptyPiece, 
        maskSetWhitePiece, maskSetBlackPiece, maskSetWhitePiece, maskSetBlackPiece,
        maskSetWhitePiece, maskSetBlackPiece, maskSetWhitePiece, maskSetBlackPiece, 
        maskSetWhitePiece, maskSetBlackPiece, maskSetWhitePiece, maskSetBlackPiece};

void setPieceAt(Position * const pos, const int sq, const int piece){
    const Int64 mask = TWOTOTHE(sq);
    pos->pieces[pos->board[sq]] ^=  mask;
    pos->pieces[piece]          |=  mask;
    pos->board[sq] = piece;
    
    maskFunctions[piece](pos->white, pos->black, sq);
}

void removePieceAt(Position * const pos, const int sq){
    const Int64 mask = TWOTOTHE(sq);
    pos->pieces[pos->board[sq]]      ^=  mask;
    pos->pieces[pos->board[sq] = 0]  |=  mask;
    
    maskSetEmptyPiece_inline(pos->white, sq);
}

void afterMove(const Position * restrict pos, const int sq1, const int sq2, 
        Position * restrict child){
    memcpy(child, pos, sizeof(Position));
    setPieceAt(child, sq2, pos->board[sq1]);
    removePieceAt(child, sq1);
    
    child->turn ^= 1;
}

void calculatePositionValue(Position * pos){
    int sq;
    pos->value[0] = 0;
    for (sq = 0; sq < 64; sq++){
        pos->value[0] += pieceValues[pos->board[sq]];
    }
    pos->value[1] = -(pos->value[0]);
}

int  getTurn(const Position * const pos){ return pos->turn; }
void setTurn(Position * const pos, int t){ pos->turn = t; }

int getBranch(Position * const pos, int i){ return pos->branch[i]; }

int  getValue(const Position * const pos){ return pos->value[0]; }
void setValue(Position * const pos, int v){ pos->value[0] = v; pos->value[1] = -v; }
