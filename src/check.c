// check.c
// 26 Aug 2007

Int64 inCheckFromKnight(Position * pos, int side){
    int kingSquare = lowestBit(pos->pieces[12 ^ side]);
    Int64 knightAttacks = knightFields[kingSquare];
    return knightAttacks & pos->pieces[5 ^ side];
}

Int64 inCheckFromSlider(Position * pos, int side){
    int kingSquare = lowestBit(pos->pieces[12 ^ side]);
    Position children[32];
    Position * endChildren, * child;
    
    Int64 occupancy[4];
    int i;
    for (i = 0; i < 4; i++)
        occupancy[i] = pos->white[i] | pos->black[i];
    
    Int64 rdests = rookDestinations(pos, kingSquare, occupancy);
    if (rdests & (pos->pieces[9 ^ side] | pos->pieces[11 ^ side]))
        return 1;
    
    /* THIS IS WRONG... DO BISHOP MOVES RIGHT FIRST...
    // now, for the bishops...
    endChildren = bishopMoves(pos, kingSquare, children, 
                    pos->white[pos->turn << 2], pos->white[(!pos->turn) << 2]);
    for (child = children; child != endChildren; child++){
        if (child->value[0] > 8000 || child->value[1] > 8000)
            return 1;
    }
    */
        
    return 0;
}

Int64 inCheckFromPawn(Position * pos, int side){
    return 0;
}

Int64 areKingsAdjacent(Position * pos){
    Int64 wkingSquare = lowestBit(pos->pieces[12]);
    return kingFields[wkingSquare] & pos->pieces[13];
}

int inCheck(Position * pos, int side){
    return (inCheckFromKnight(pos, side) || inCheckFromSlider(pos, side) 
        || inCheckFromPawn(pos, side) || areKingsAdjacent(pos)) != 0ULL;
}

