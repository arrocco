/*  pieces.c
    28 Feb 2007 */

/*
    This file is part of Arrocco, which is Copyright 2007 Thomas Plick 
    (tomplick 'at' gmail.com).

    Arrocco is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Arrocco is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdbool.h>
    
const int EMPTY = 0, PAWN = 2,   KNIGHT = 4, BISHOP = 6, 
          ROOK = 8,  QUEEN = 10, KING = 12;
          
const int pieceValues[14] = {0, 0, 10, -10, 30, -30, 30, -30, 50, -50, 90, -90, 
            8888, -8888};
          
#define WHITE(x) (x)
#define BLACK(x) (x+1)

inline bool isWhite(int x){
    return x && ((~x) & 1);
}

inline bool isBlack(int x){
    return x & 1;
}


inline int opponent(int x){
    return x ^ 1;
}

int promoteTo(int pawn, int piece){
    return (pawn & 1) | piece;
}

bool sameColor(int x, int y){
    if (x == 0 || y == 0){
        return false;
    } else {
        return !((x ^ y) & 1);
    }
}

bool sameColorNotZero(int x, int y){
    return !((x ^ y) & 1);
}
