/*  main.c
    2 Mar 2007 */

/*
    This file is part of Arrocco, which is Copyright 2007 Thomas Plick 
    (tomplick 'at' gmail.com).

    Arrocco is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Arrocco is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "all.c"

static Position * pos;
static Position children[20 * 512];

int main(int argc, char *argv[])
{
    int offswitch, d, c, n, i;
    Int64 nodeCount;

    pos = malloc(sizeof(Position));
    newPosition(pos);

    int pieceArrangement[8] = {ROOK, KNIGHT, BISHOP, QUEEN, 
                               KING, BISHOP, KNIGHT, ROOK};
    for (c = 0; c < 8; c++){
        setPieceAt(pos, 1 * 8 + c, WHITE(PAWN));
        setPieceAt(pos, 6 * 8 + c, BLACK(PAWN));
        setPieceAt(pos,         c, WHITE(pieceArrangement[c]));
        setPieceAt(pos, 7 * 8 + c, BLACK(pieceArrangement[c]));
    }

    for (d = 0; d <= 9; d++){
        nodeCount = 0ULL;
        offswitch = 0;
        int value = alphaBeta(pos, children, d, -7777777, 7777777, 
                        &nodeCount, &offswitch);
        printf("%2d %4d   %llu\n", d, value, nodeCount);
    }

    return 0;
}
