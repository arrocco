# cpu.py
# August 2007

"""
    This file is part of Arrocco, which is Copyright 2007 Thomas Plick 
    (tomplick 'at' gmail.com).

    Arrocco is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Arrocco is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os, subprocess

def count_cpus_x():
    command = ['grep', '-cw', '^processor', '/proc/cpuinfo']
    proc = subprocess.Popen(command, stdout = subprocess.PIPE)
    return int(proc.communicate()[0].strip())

def count_cpus():
    try:
        return count_cpus_x()
    except:
        return 1
