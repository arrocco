# search.py
# 28 June 2007

"""
    This file is part of Arrocco, which is Copyright 2007 Thomas Plick 
    (tomplick 'at' gmail.com).

    Arrocco is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Arrocco is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import time, ctypes, thread, Queue, threading

from lib import lib
from position import *

pvCache = {}

def updatePVCache(pv):
    for (a, b) in zip(pv, pv[1:]):
        pvCache[a.fen()] = b


def alphaBetaInC(pos, depth,
            alpha, beta, stopper = None, childArray = None):
    counter = ctypes.c_long(1)
    if childArray is None:
        childArray = (Position * (depth * 32))()
        
    value = lib.alphaBeta(ctypes.byref(pos), childArray, depth, 
                    alpha, beta, ctypes.byref(counter), ctypes.byref(stopper))
                    
    # get the PV...
    pv = L = [pos]
    node = pos
    for i in range(depth):
        try:
            node = node.children()[pos.getBranch(i)]
            L.append(node)
        except: break
        
    return dict(value = value, pv = pv,
               nodeCount = counter.value)

def alphaBeta(pos, depth, alpha = -99999, beta = 99999, splits = 0,
        stopper = None, top = False, parentProc = None):
    a = time.time()
    
    pos.offswitch.value = 0
    result = alphaBetaParallel(pos, depth, alpha, beta, splits, 
                pos.offswitch, parentProc)
    pvCache.clear()
    updatePVCache(result['pv'])
        
    b = time.time()
    result['time'] = int(b - a) + 1
    return result

    
# parallel stuff

import spawn
def alphaBetaParallel(pos, depth, alpha, beta, splits, stopper, parentProc = None):
    kids = pos.children()
    if depth == 0 or len(kids) == 0 or stopper.value:
        return alphaBetaInC(pos, 0, alpha, beta, stopper)
    
    running = []
    results = Queue.Queue()        
    nodeCount = 1
    bestPV = []
    
    q = Queue.Queue()
    if pos.fen() in pvCache:
        favorite = pvCache[pos.fen()]
        kids[kids.index(favorite)] = None
        q.put(favorite)
        at_a_time = 1
    elif splits <= 0:
        return alphaBetaInC(pos, depth, alpha, beta, stopper)
    else:  at_a_time = 2

    for k in kids:
        if k is not None: q.put(k)

    rfq = 0
    try:
        while rfq < len(kids):
            if len(running) < at_a_time:
                try:
                    next = q.get_nowait().copy()
                    stopper = ctypes.c_int(0)
                    def calc():
                        ret = next, proc, alphaBeta(next, depth - 1, -beta, -alpha, 
                                        splits - at_a_time + 1, stopper, parentProc)
                        if stopper.value == 0:  results.put(ret)
                    def stop(): stopper.value = 1

                    if not parentProc:
                        parentProc = spawn  # module spawn
                    proc = parentProc.spawn(calc, stop)

                    running.append(proc)
                    proc.position, proc.alpha = next, alpha
                    proc.start()
                except Queue.Empty: pass
                
            try:
                resChild, resProc, result = results.get(True, .05)
                try:
                    rfq += 1
                    if splits > 0:  at_a_time = 2
                    running.remove(resProc)
                except ValueError: pass
            except Queue.Empty:
                continue
            
            value = -result['value']
            nodeCount += result['nodeCount']

            for proc in running:
                if proc.position == resChild:
                    proc.stop()
                    running.remove(proc)

            if value >= beta:
                alpha = beta; break
            elif value > alpha:
                alpha = value
                bestPV = [pos] + result['pv']

                # evaluate other pos (if there is one) with this alpha
                for proc in running:
                    insertAtFront(proc.position, q)
    except KeyboardInterrupt:
        for kid in running: kid.stop()
        raise
    
    for kid in running: kid.stop()
    return dict(value = alpha, nodeCount = nodeCount, pv = bestPV)


def insertAtFront(element, queue):
    newQ = Queue.Queue()
    while not queue.empty(): newQ.put(queue.get())
    queue.put(element)
    while not newQ.empty(): q.put(newQ.get())


