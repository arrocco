# lib.py
# 1 Mar 2007

"""
    This file is part of Arrocco, which is Copyright 2007 Thomas Plick 
    (tomplick 'at' gmail.com).

    Arrocco is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Arrocco is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import ctypes
int64 = ctypes.c_ulonglong

from bits import *


def load():
    lib = ctypes.cdll.LoadLibrary("./libarrocco.so")
    lib.makeChildren.restype = ctypes.c_int
    lib.getTurn.restype = ctypes.c_int
        
    return lib
    
        
lib = load()
