# libgen.py
# 5 Mar 2007

"""
    This file is part of Arrocco, which is Copyright 2007 Thomas Plick 
    (tomplick 'at' gmail.com).

    Arrocco is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Arrocco is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from bits import *
import re

j = ',\n    '


print "// Automatically generated from libgen.py"


def stringArray(array):
    s = repr(list(array))
    s = s.replace('(', '[').replace(')', ']')
    s = s.replace('[', '{')
    s = s.replace(']', '}')
    s = s.replace('},', '},\n')
    return s
    
intArray = stringArray
def longArray(array):
    s = stringArray(array)
    return re.sub(r'(\d+)L?', r'\1ULL' + '\n', s)


def printKingFields():
    k = [bitfieldForBits(bitsForKing(sq)) for sq in range(64)]
    s = stringArray(k)
    s = re.sub(r'(\d+)L?', r'\1ULL' + '\n', s)
    print 'const Int64 kingFields[64] = %s;' % s
printKingFields()

def printKnightFields():
    k = [bitfieldForBits(bitsForKnight(sq)) for sq in range(64)]
    s = stringArray(k)
    s = re.sub(r'(\d+)L?', r'\1ULL' + '\n', s)
    print 'const Int64 knightFields[64] = %s;' % s
printKnightFields()

b1 = [bitsForPawnMove(sq,  1, 1) for sq in range(64)]
b2 = [bitsForPawnMove(sq, -1, 6) for sq in range(64)]
s1 = [str(bitfieldForBits(x))+"ULL" for x in b1]
s2 = [str(bitfieldForBits(x))+"ULL" for x in b2]

print 'const Int64 pawnMoveFields[2][64] = {{%s}, {%s}};' % (',\n    '.join(s1), ',\n    '.join(s2))
print

b1 = [bitsForPawnCapture(sq,  1) for sq in range(64)]
b2 = [bitsForPawnCapture(sq, -1) for sq in range(64)]
s1 = [str(bitfieldForBits(x))+"ULL" for x in b1]
s2 = [str(bitfieldForBits(x))+"ULL" for x in b2]

print 'const Int64 pawnCaptureFields[2][64] = {{%s}, {%s}};' % (',\n    '.join(s1), ',\n    '.join(s2))
print

k = [((sq >> 3) << 4) + (sq & 7) for sq in range(64)]
s = [str(x) for x in k]

print 'const int x88FromDest[64] = {%s};' % (',\n    '.join(s))
print

def strrr(n):
    return 2**64 + ~(1 << n)


c1 = [strrr(x)  for x in range(64)]
c2 = [strrr(8 * c + r)  for r in range(8) for c in range(8)]
c3 = [strrr(8 * r + c)  for d in range(-7, 8) for r in range(8) for c in range(8)
                             if r - c == d]
c4 = [strrr(8 * r + c)  for d in reversed(range(0, 15)) for r in range(8) for c in range(8)
                             if r + c == d]
s = stringArray(zip(c1, c2, c3, c4, c1, c2, c3, c4))
s = s = re.sub(r'(\d+)L?', r'\1ULL' + '\n', s)
print 'const Int64 rotMasks[64][8] = %s;\n' % s

c2 = [8 * c + r  for r in range(8) for c in range(8)]
c3 = [8 * r + c  for d in range(-7, 8) for r in range(8) for c in range(8)
                                         if r - c == d]
c4 = [8 * r + c  for d in reversed(range(0, 15)) for r in range(8) for c in range(8)
                                         if r + c == d]
print 'const Int64 rotFunctions[4][64] = %s;\n' % stringArray([range(64), c2, c3, c4])


print 'const int rookHorizontalShift[64] = %s;\n' % \
        stringArray([n & ~7 for n in range(64)])
print 'const int rookVerticalShift[64] = %s;\n' % \
        stringArray([(n & 7) * 8 for n in range(64)])

def getIndex(n, shifts):
    i = shifts.index(n)
    while i and shifts[i-1] < shifts[i]:
        i -= 1
    return i
acuteShifts = [getIndex(i, c3) for i in range(64)]
graveShifts = [getIndex(i, c4) for i in range(64)]
print 'const int bishopAcuteShift[64] = %s;\n' % stringArray(acuteShifts)
print 'const int bishopGraveShift[64] = %s;\n' % stringArray(graveShifts)

s = stringArray([ [bitfieldForBits(bitsForHorizontalRookDests(sq, occ)) for occ in xrange(256)]
                       for sq in range(64)])
s = re.sub(r'(\d+)L?', r'\1ULL' + '\n', s)
print 'const Int64 rookHorizontalDests[64][256] = %s;\n' % s

s = stringArray([ [bitfieldForBits(bitsForVerticalRookDests(sq, occ)) for occ in xrange(256)]
                       for sq in range(64)])
s = re.sub(r'(\d+)L?', r'\1ULL' + '\n', s)
print 'const Int64 rookVerticalDests[64][256] = %s;\n' % s


s = stringArray(1 << i for i in range(64))
s = re.sub(r'(\d+)L?', r'\1ULL' + '\n', s)
print 'const Int64 twoToThe[64] = %s;\n' % s

def mapSquare(x, default):
    (r, c) = x
    if r in range(8) and c in range(8):
        return 8 * r + c
    else:
        return default

print 'const Int64 pawnsThatAttack[64][2][2] = {'
for sq in range(64):
    print '{'
    r1, c1 = divmod(sq, 8)
    for i in range(2):
        r2 = r1 + {0 : 1, 1 : -1}[i]
        squares = [(r2, c1 + k) for k in (-1, 1)]
        squares = [mapSquare(x, sq) for x in squares]
        print '{%s, %s}' % (squares[0], squares[1]),
        if i == 0: print ',',
    print '}',
    if sq < 63: print ','
print '};'

