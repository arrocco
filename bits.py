# bits.py
# 1 Mar 2007
# Make bitfields for piece moves.

"""
    This file is part of Arrocco, which is Copyright 2007 Thomas Plick 
    (tomplick 'at' gmail.com).

    Arrocco is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Arrocco is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

def bitsForKing(sq):
    r1, c1 = divmod(sq, 8)
    return [i for i in range(64) for (r2, c2) in [divmod(i, 8)] 
                if max(abs(r1-r2), abs(c1-c2)) == 1]

def bitsForKnight(sq):
    r1, c1 = divmod(sq, 8)
    return [i for i in range(64) for (r2, c2) in [divmod(i, 8)] 
                if abs(r1-r2) * abs(c1-c2) == 2]

def bitfieldForBits(bits):
    return sum(2**i for i in bits if i >= 0 and i < 64)
    
def bitsForPawnMove(sq, direc, firstRow):
    L = [sq + direc * 8]
    if sq // 8 == firstRow:
        L.insert(0, sq + direc * 16)
    return L
    
def bitsForPawnCapture(sq, direc):
    r, c = divmod(sq, 8)
    L = []
    if c > 0:  L.append((r+direc) * 8 + (c - 1))
    if c < 7:  L.append((r+direc) * 8 + (c + 1))
    return L
    
def bitsForHorizontalRookDests(sq, occupancy):
    r, c1 = divmod(sq, 8)
    occupiedColumns = [c for c in range(8) if occupancy & (1 << c)]
    for c2 in range(c1 + 1, 8):
        yield 8 * r + c2
        if c2 in occupiedColumns:
            break
    for c2 in range(c1 - 1, -1, -1):
        yield 8 * r + c2
        if c2 in occupiedColumns:
            break

def bitsForVerticalRookDests(sq, occupancy):
    r1, c = divmod(sq, 8)
    occupiedRows = [r for r in range(8) if occupancy & (1 << r)]
    for r2 in range(r1 + 1, 8):
        yield 8 * r2 + c
        if r2 in occupiedRows:
            break
    for r2 in range(r1 - 1, -1, -1):
        yield 8 * r2 + c
        if r2 in occupiedRows:
            break
