#!/usr/bin/env python2.5
# arrocco.py
# main file

"""
    This file is part of Arrocco, which is Copyright 2007 Thomas Plick 
    (tomplick 'at' gmail.com).

    Arrocco is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Arrocco is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import getopt, sys, cpu
from position import *
from search import *
from partest import *

def intlog(x):
    n = 0
    while x > 1:
        x = int(x/2)
        n += 1
    return n

options, args = getopt.getopt(sys.argv[1:], "a24", ["cpus="])
options = dict(options)

fen = (args + [ipos.fen()])[0]
pos = position(fen)

measuredCpus = cpu.count_cpus()

cpus = 1
if '-a' in options:  cpus = measuredCpus
elif '-2' in options: cpus = 2
elif '-4' in options: cpus = 4
else:
    try:
        cpus = int(options['--cpus'])
    except:
        cpus = 1

splits = intlog(cpus)
print "Using %s CPUs." % (2**splits)
if measuredCpus > cpus:
    print "   (Note: You specified %s CPU%s, but your system appears to have %s." % \
            (cpus, '' if cpus == 1 else 's', measuredCpus)
    print "    If you would like to use them, specify `--cpus=%s' on the command line.)" % \
            (measuredCpus)

def mainidea():
    print pos
    for d in range(3, 100):
        x = alphaBeta(pos, d, top = True, splits = splits)
        print "\nDepth %d:" % d
        print "    ", removePV(x)
        print "  ", algebraicForPV(x['pv'])

if __name__ == '__main__':
    thread.start_new_thread(mainidea, ())
    while 1:
        time.sleep(1000)
