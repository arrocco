default:   prelude
	gcc -std=gnu99 -fPIC -shared src/all.c -o libarrocco.so -O6 -DASM \
	        -funroll-loops -fprefetch-loop-arrays \
	        -mmmx -msse -m3dnow
	echo Success.
slow:   prelude	
	gcc -std=gnu99 -fPIC -shared src/all.c -o libarrocco.so -O6
	echo Success.
exe:   prelude
	gcc -std=gnu99 -DASM src/main.c -pg -g -O6 -S
	gcc -std=gnu99 -DASM src/main.c -pg -g -O6 -o a.out
	echo Success.
dist:
	python freeze/freeze.py -o freeze_temp -X pydoc arrocco.py
	cd freeze_temp && make && cd ..
	cp freeze_temp/arrocco . && strip arrocco

prelude: _bits.c
_bits.c:  libgen.py
	python libgen.py print > _bits.c
	cp _bits.c src/
printout:
	./gather.py > printout.tex
	pdflatex printout.tex
clean:
	rm _bits.c || true
	rm src/_bits.c || true

