# loop.py
# 21 August 2007 por la tarde

import time, sys, thread, Queue

def f(n):
    for i in xrange(n):
        pass
    q.get_nowait()
        
# main
threads, iterations = int(sys.argv[1]), int(sys.argv[2])

q = Queue.Queue()
for i in range(threads):
    q.put(None)
for i in range(threads):
    thread.start_new_thread(f, (iterations,))

while not q.empty():
    time.sleep(1)
