#!/usr/bin/python
# gather.py
# 29 Nov 2006
# Gather together all the .py files into a Latex document.
# If you are reading this in the source printout, I hope you're enjoying
#       the self-referential madness.  I know I am.

# invoke in source directory as:  ./gather.py > output.tex

import sys

print r"""
\documentclass{article}
\usepackage{fullpage}
\usepackage[left=1.0in,right=1.0in,top=1.0in,bottom=1.0in,nohead]{geometry}
\begin{document}
"""

import glob
filenames = glob.glob("*.py") + glob.glob("src/*.c") + glob.glob("src/*.h") + ["Makefile"]
filenames.sort()
filenames = [x for x in filenames if "_" not in x]

for i, filename in enumerate(filenames):
    # if i > 1:
    #    print r"\pagebreak"

    # print "\n%scenter{%s}\n" % ('\\', filename.replace('_', r'\_'))
    print "\n%ssection{%s}\n" % ('\\', filename.replace('_', r'\_'))
    print r"""
\begin{%s}
%s
\end{%s}
""" % ('verbatim', file(filename, 'rU').read().replace('\t', '    '), 'verbatim')
    
print r"\end{document}"
