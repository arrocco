# __init__.py
# 14 Aug 2007

import time, weakref, gc
import pool as threadmod

class Process:
    def __init__(self, computation, stopper):
        self.container = [None]
        self.computation = computation
        self.stopper = stopper
    
    def run(self):
        try:
            self.container[0] = self.computation()
        except Exception, e:
            self.container[0] = repr(e)

class MyList(list):
    def __init__(self, x):
        list.__init__(self)
        self.append(x)

class MyObject: pass

def spawn(computation, stopper):
    proc = Process(computation, stopper)

    token = MyObject()    
    wref = weakref.ref(token, lambda arg: stopper())

    class Retriever:
        def __init__(self):
            self.children = []
            
        def __call__(self):
            x = proc.container[0]
            if x is None: return ('working', None)
            else:   return ('done', x)

        def __repr__(self):
            return '<Retriever holding value %s>' % str(self())

        def stop(self):
            stopper()
            for kid in self.children:
                kid.stop()
                
        def spawn(self, comp, stopr):
            p = globals().spawn(comp, stopr)
            self.children.append(p)

        def start(self):
            threadmod.start_new_thread(proc.run, ())

    r = Retriever()
    r.saving_ref = token
    del token
    return r

def fib(n, go):
    if not go[0]:
        raise Exception, "Cancelled"
    elif n <= 2:
        return 1
    else:
        return fib(n - 1, go) + fib(n - 2, go)

def test(n):
    go = [True]
    def stop(): go[0] = False
    return spawn(lambda: fib(n, go), stop)

