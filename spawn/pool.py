# pool.py
# 11 Sep 2007

import Queue, thread
q = Queue.Queue()
inited = False

def initialize():
    global inited; inited = True
    for i in range(20):
        thread.start_new_thread(persistent_worker, ())
        
def persistent_worker():
    while True:
        computation = q.get()
        computation()
        
def start_new_thread(f, args, kwargs = None):
    if not inited: initialize()
    if not kwargs: kwargs = {}
    q.put(lambda: f(*args, **kwargs))

