# position.py
# 1 Mar 2007

"""
    This file is part of Arrocco, which is Copyright 2007 Thomas Plick 
    (tomplick 'at' gmail.com).

    Arrocco is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Arrocco is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import ctypes, time, threading, thread, copy, Queue

from lib import lib
SIZE_OF_POSITION = lib.sizeOfPosition()
c_int = ctypes.c_int
c_short = ctypes.c_short
int64 = ctypes.c_ulonglong

charForPiece = "  PpNnBbRrQqKk"

for r in range(8):
    for c in range(8):
        exec "%c%c = 8 * %d + %d" % ("abcdefgh"[c], "12345678"[r], r, c) in globals(), globals()
        

class Position(ctypes.Structure):
    _fields_ = [("opaque", ctypes.c_byte * SIZE_OF_POSITION)]
    
    def __init__(self):
        ctypes.Structure.__init__(self)
        self.offswitch = ctypes.c_int(0)
                
    def getTurn(self): return lib.getTurn(ctypes.pointer(self))
    def setTurn(self, x): lib.setTurn(ctypes.pointer(self), x)
    turn = property(getTurn, setTurn)
    
    def getValue(self): return lib.getValue(ctypes.pointer(self))
    def setValue(self, x): lib.setValue(ctypes.pointer(self), x)
    value = property(getValue, setValue)
    
    def getBranch(self, i): return lib.getBranch(ctypes.byref(self), i)
                
    def __getitem__(self, (r, c)):
        return lib.getPieceAt(ctypes.pointer(self), 8 * r + c)
        
    def __setitem__(self, (r, c), v):
        lib.setPieceAt(ctypes.pointer(self), 8 * r + c, v)
        lib.calculatePositionValue(ctypes.byref(self))
        
    def fen(self):
        def rowString(r):  return ''.join(charForPiece[self[r, c]] for c in range(8))
        boardString = '/'.join(rowString(r) for r in range(7, -1, -1)).replace(' ', '1')
        for i in range(8, 1, -1):  boardString = boardString.replace('1' * i, str(i))
        return "%s %s" % (boardString, ['w', 'b'][self.turn & 1])
        
    def setFen(self, fen):
        parts = fen.split(" ") + ["w"]
        boardString, turn = parts[0:2]
        
        boardString = boardString.replace('/', '') + (' ' * 64)
        for i in range(1, 9):  boardString = boardString.replace(str(i), ' ' * i)
        for n in range(64):
            r, c = divmod(n, 8)
            r = 7 - r
            piece = charForPiece.find(boardString[n])
            if piece < 0:  piece = 0
            self[r, c] = piece
        
        lib.setTurn(ctypes.pointer(self), 0 if turn == 'w' else 1)

    def __repr__(self):  return "position(%r)" % self.fen()
    def __str__(self):  return ''.join(self.display())
    def display(self):
        for r in reversed(range(8)):
            for c in range(8):
                yield charForPiece[self[r, c]].replace(' ', '_')
            yield '\n'
    
    def children(self):
        childArray = (Position * 512)()
        numberOfChildren = lib.makeChildren(ctypes.byref(self), ctypes.byref(childArray))
        return childArray[0:numberOfChildren]
    def legalChildren(self):
        return [child for child in self.children() 
                    if not lib.inCheck(ctypes.byref(child), self.getTurn())]
        
    def childrenGen(self):
        childArray = (Position * 512)()
        numberOfChildren = lib.makeChildren(ctypes.byref(self), ctypes.byref(childArray))
        i = 0
        for child in childArray:
            if i >= numberOfChildren:
                break
            yield child
            i += 1
        

    def afterMove(self, sq1, sq2, promote=None):
        copy = Position()
        lib.afterMove(ctypes.byref(self), sq1, sq2, ctypes.byref(copy))
        return copy
        
    def afterPass(self):
        copy = self.copy(); copy.turn ^= 1; return copy
        
    def copy(self):
        copy = Position()
        ctypes.memmove(ctypes.byref(copy), ctypes.byref(self), SIZE_OF_POSITION)
        return copy
        
    def __cmp__(self, other):  return cmp(self.fen(), other.fen())
    def __hash__(self): return hash(self.fen())
    
    def unstop(self): self.offswitch.value = 0
    def stop(self):   self.offswitch.value = 1

        
def position(s):
    pos = Position(); pos.setFen(s); return pos
    
allSquares = [(r, c) for r in range(8) for c in range(8)]
def algebraicForPV(pv):
    def f((r, c)):  return "abcdefgh"[c] + "12345678"[r]
    def g(x):  return charForPiece[x].upper()
    
    L = []
    while len(pv) > 1:
        parent, child = pv[:2]
        x = [sq for sq in allSquares if parent[sq] != child[sq]]
        try:
            if child[x[0]] > 0:
                x.reverse()
            L.append(g(parent[x[0]]) + f(x[0]) + f(x[1]))
        except: break
            
        pv = pv[1:]
    return L

ipos = position("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq")
pos12 = position("k7/8/KR")
